// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
angular.module('starter', [
  'ionic',
  'starter.controllers',
  'starter.services',
  'ngCordova'
])

.run(function ($ionicPlatform, $cordovaSQLite) {
  $ionicPlatform.ready(function() {
    if(window.cordova && window.cordova.plugins.Keyboard) {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);

      // Don't remove this line unless you know what you are doing. It stops the viewport
      // from snapping when text inputs are focused. Ionic handles this internally for
      // a much nicer keyboard experience.
      cordova.plugins.Keyboard.disableScroll(true);
    }
    if(window.StatusBar) {
      StatusBar.styleDefault();
    }
  });
})

.config(function ($stateProvider, $urlRouterProvider) {
  $stateProvider
  .state('intro', {
      url: '/intro',
      templateUrl: 'templates/intro.html',
      controller: 'IntroCtrl'
  })
  .state('login', {
      url: '/login',
      cache:false,
      templateUrl: 'templates/login.html',
      controller: 'LoginCtrl'
  })
  .state('search-applicant', {
      url: '/search-applicant',
      templateUrl: 'templates/search-applicant.html',
      controller: 'ApplicantCtrl'
  })
  .state('profile', {
      url: '/profile/:id',
      templateUrl: 'templates/profile.html',
      controller: 'ApplicantCtrl'
  })
  .state('userprofile', {
      url: '/userprofile',
      cache:false,
      templateUrl: 'templates/userprofile.html',
      controller: 'UserCtrl'
  })
  $urlRouterProvider.otherwise('/intro');
});
