angular.module('starter.services', [])

.factory('LocalStorage', ['$window', function ($window) {
    var prefix = 'myra_';

    return {

        set: function (key, value) {
            $window.localStorage[prefix + key] = value;
        },

        get: function (key, defaultValue) {
            return $window.localStorage[prefix + key] || null;
        },

        setObject: function (key, value) {
            $window.localStorage[prefix + key] = JSON.stringify(value);
        },

        getObject: function (key) {
            return JSON.parse($window.localStorage[prefix + key] || '{}');
        },

        remove: function (key) {
            $window.localStorage.removeItem(prefix + key);
        },

        clear: function () {
            $window.localStorage.clear();
        }

    }
}])

.factory("transformRequestAsFormPost", function () {

    function transformRequest(data, getHeaders) {
        var headers = getHeaders();
        headers["Content-type"] = "application/x-www-form-urlencoded";
        return (serializeData(data));
    }
    return (transformRequest);

    /** Private method */
    function serializeData(data) {
        if (!angular.isObject(data)) {
            return ((data == null) ? "" : data.toString());
        }
        var buffer = [];
        // Serialize each key in the object.
        for (var name in data) {
            if (!data.hasOwnProperty(name)) {
                continue;
            }
            var value = data[name];
            buffer.push(
                encodeURIComponent(name) +
                "=" +
                encodeURIComponent((value == null) ? "" : value)
            );
        }
        // Serialize the buffer and clean it up for transportation.
        var source = buffer.join("&").replace(/%20/g, "+");
        return (source);
    }
})

.factory('Toast', function ($rootScope, $timeout, $ionicPopup, $cordovaToast) {

    return {
        show: function (message, duration, position) {
            message = message || "There was a problem...";
            duration = duration || 'short';
            position = position || 'bottom';

            if (!!window.cordova) {
                $cordovaToast.show(message, duration, position);
            } else {

                if (!duration) {
                    duration = 5000;
                }

                var myPopup = $ionicPopup.show({
                    template: "<div class='toast'>" + message + "</div>",
                    scope: $rootScope,
                    buttons: []
                });

                $timeout(function () {
                    myPopup.close();
                }, duration);
            }
        }
    };
})

.factory('Obj', function ($rootScope) {

    function size(obj) {
        var size = 0,
            key;
        for (key in obj) {
            if (obj.hasOwnProperty(key)) size++;
        }
        return size;
    }

    return {
        size: size
    };
})

.factory('Dialog', function ($ionicPopup, $cordovaDialogs, $q) {

    function alert(message, title, button, callback) {

        if (!window.cordova) {
            $ionicPopup.alert({
                title: title,
                template: message
            }).then(callback);
        } else {
            $cordovaDialogs.alert(message, title, button).then(callback);
        }

    }

    function confirm(title, template, callback, buttons, ok, cancel) {
        okText = ok || "OK";
        cancelText = cancel || "Cancel";
        /*      if (buttons) {
                buttons.forEach(function(item){
                  console.log(item);
                });
              }
              else {
                okText = "OK";
                cancelText = "Cancel";
              }*/
        if (!window.cordova) {
            var confirmPopup = $ionicPopup.confirm({
                title: title,
                template: template,
                cancelText: cancelText,
                okText: okText
            }).then(callback);
        } else {
            buttonArray = buttons;
            $cordovaDialogs.confirm(template, title, buttonArray)
                .then(function (buttonIndex) {
                    callback(buttonIndex);
                });

        }

    }

    function prompt(message, title, callback, ok, cancel) {
        okText = ok || "OK";
        cancelText = cancel || "Cancel";
        if (!window.cordova) {
            var promptPopup = $ionicPopup.prompt({
                title: title,
                subTitle: message,
                inputType: 'text',
                okText: okText,
                cancelText: cancelText
            }).then(callback);
        } else {
            buttonArray = [okText, cancelText];
            $cordovaDialogs.prompt(message, title, buttonArray)
                .then(callback);
        }

    }
    return ({
        alert: alert,
        confirm: confirm,
        prompt: prompt
    });
})

.factory('Network', function ($cordovaNetwork, Dialog) {
    function isOfline() {
        if (window.cordova) {
            if ($cordovaNetwork.isOffline()) {
                Dialog.alert('No network connection.', 'Network Connection', 'Ok', function () {});
                return true;
            }
        }
    }
    return ({
        isOfline: isOfline
    });
})
    
.factory('DBA', function ($cordovaSQLite, $q, $ionicPlatform) {
    var self = this;

    // Handle query's and potential errors
    self.query = function (query, parameters) {
        parameters = parameters || [];
        var q = $q.defer();

        $ionicPlatform.ready(function () {
            $cordovaSQLite.execute(db, query, parameters)
                .then(function (result) {
                    q.resolve(result);
                }, function (error) {
                    console.warn('I found an error');
                    console.warn(error);
                    console.warn(query);
                    console.warn(parameters);
                    q.reject(error);
                });
        });
        return q.promise;
    }

    // Proces a result set
    self.getAll = function (result) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }
        return output;
    }

    // Proces a single result
    self.getById = function (result) {
        var output = null;
        if (result.rows.length > 0) {
            output = angular.copy(result.rows.item(0));
        }

        return output;
    }

    self.getByField = function (result) {
        var output = null;
        if (result.rows.length > 0) {
            output = angular.copy(result.rows.item(0));
        }

        return output;
    }

    self.getAllByField = function (result) {
        var output = [];

        for (var i = 0; i < result.rows.length; i++) {
            output.push(result.rows.item(i));
        }
        return output;
    }

    // self.getbyDateRange = function (result) {
    //     var output = [];

    //     for (var i = 0; i < result.rows.length; i++) {
    //         output.push(result.rows.item(i));
    //     }
    //     return output;
    // }

    return self;
})

.factory('DB', function ($cordovaSQLite, DBA) {
    var self = this;

    self.all = function (table, order, sort) {

        if (order!=1) {
            var thisorder = ' order by ' + order + ' ' + sort + '';
        } else {
            var thisorder = '';
        }

        return DBA.query("SELECT * FROM " + table + thisorder)
            .then(function (result) {
                return DBA.getAll(result);
            });
    }

    self.get = function (table, id) {
        var parameters = [id];
        return DBA.query("SELECT * FROM " + table + " WHERE id = (?)", parameters)
            .then(function (result) {
                return DBA.getById(result);
            });
    }

    self.getByField = function (table, field, value) {
        var parameters = [value];
        return DBA.query("SELECT * FROM " + table + " WHERE " + field + " = (?)", parameters)
            .then(function (result) {
                return DBA.getByField(result);
            });
    }

    self.getAllByField = function (table, field, value) {
        var parameters = [value];
        return DBA.query("SELECT * FROM " + table + " WHERE " + field + " = (?)", parameters)
            .then(function (result) {
                return DBA.getAllByField(result);
            });
    }

    self.getbyDateRange = function (table, field, value) {
        var parameters = [value];
        return DBA.query("SELECT * FROM " + table + " WHERE " + field + " >= (?) and " + field + " <= (?)", parameters)
            .then(function (result) {
                return DBA.getAll(result);
            });
    }

    self.displayQuery = function (query, parameters) {
        return DBA.query(query, parameters)
            .then(function (result) {
                return DBA.getAll(result);
            });
    }

    self.query = function (query, parameters) {
        return DBA.query(query, parameters)
    }


    self.updateTimestamp = function (table, id, date) {
        return DBA.query("UPDATE " + table + " set updated_at='" + date + "', sync_date='" + date + "' WHERE id = (?)", [id]);
    }

    self.remove = function (table, field, value) {
        var parameters = [value];
        return DBA.query("DELETE FROM " + table + " WHERE " + field + " = (?)", parameters);
    }


    return self;
})

.factory('Convert', function ($ionicPopup, $cordovaDialogs, $q) {

    function convertEpoch(val) {
        var meridian = ['AM', 'PM'];
        var hours = parseInt(val / 3600);
        var minutes = (val / 60) % 60;
        var hoursRes = hours > 12 ? (hours - 12) : hours;
        var currentMeridian = meridian[parseInt(hours / 12)];
        return prependZero(hoursRes) + ":" + prependZero(minutes) + " " + currentMeridian;
    }

    function prependZero(param) {
        if (String(param).length < 2) {
            return "0" + String(param);
        }
        return param;
    }

    return ({
        epoch: convertEpoch
    });
})

.factory('Photos', function ($cordovaFile, $q) {

    function base64toBlob(base64Data, contentType) {
        contentType = contentType || '';
        var sliceSize = 1024;
        var byteCharacters = atob(base64Data);
        var bytesLength = byteCharacters.length;
        var slicesCount = Math.ceil(bytesLength / sliceSize);
        var byteArrays = new Array(slicesCount);

        for (var sliceIndex = 0; sliceIndex < slicesCount; ++sliceIndex) {
            var begin = sliceIndex * sliceSize;
            var end = Math.min(begin + sliceSize, bytesLength);

            var bytes = new Array(end - begin);
            for (var offset = begin, i = 0; offset < end; ++i, ++offset) {
                bytes[i] = byteCharacters[offset].charCodeAt(0);
            }
            byteArrays[sliceIndex] = new Uint8Array(bytes);
        }
        return new Blob(byteArrays, {
            type: contentType
        });
    }

    function write(image, file_name) {
        var base64Blob = base64toBlob(image, 'image/jpeg')
        var q = $q.defer();
        $cordovaFile.writeFile(cordova.file.dataDirectory, file_name, base64Blob, true)
            .then(function (result) {
                q.resolve(result);
            }, function (error) {
                // Dialog.alert('Image Error. Please try again.', 'Image Error');
            });
        return q.promise;
    }

    return ({
        write: write
    });
})

.factory('ClockSrv', function ($interval) {
    'use strict';
    var service = {
        clock: addClock,
        cancelClock: removeClock
    };

    var clockElts = [];
    var clockTimer = null;
    var cpt = 0;

    function addClock(fn) {
        var elt = {
            id: cpt++,
            fn: fn
        };
        clockElts.push(elt);
        if (clockElts.length === 1) {
            startClock();
        }
        return elt.id;
    }

    function removeClock(id) {
        for (var i in clockElts) {
            if (clockElts[i].id === id) {
                clockElts.splice(i, 1);
            }
        }
        if (clockElts.length === 0) {
            stopClock();
        }
    }

    function startClock() {
        if (clockTimer === null) {
            clockTimer = $interval(function () {
                for (var i in clockElts) {
                    clockElts[i].fn();
                }
            }, 1000);
        }
    }

    function stopClock() {
        if (clockTimer !== null) {
            $interval.cancel(clockTimer);
            clockTimer = null;
        }
    }

    return service;
});