angular.module('starter.controllers', ['web-api'])

.controller('LoginCtrl', function ($scope, $rootScope, $state, $ionicLoading, $cordovaNetwork, $timeout, DB, Obj, WebApi, Dialog, LocalStorage) {
    var result = LocalStorage.getObject('apidata');
    document.addEventListener("deviceready", function () {
        /* Listener for syncing when device become online */
        var type = $cordovaNetwork.getNetwork();
        $scope.online = type;
        if(type == 'none'){
        	Dialog.alert('No internet connection.', 'Error');
        }

        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.online = networkState;
        });

        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
          	$scope.online = networkState;
          	$ionicLoading.show({
                content: 'Loading',
                showBackdrop: true,
                maxWidth: 50,
                showDelay: 0
            });
          	Dialog.alert('Connection lost.', 'Error');
          	$timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        });

    }, false);

    $scope.data = {};
    $scope.data.url = LocalStorage.get('apiurl') || 'https://www.myra.work';
	$scope.data.email_address = '';
    $scope.data.password = '';
    $scope.login = function(form){
		if($scope.online == 'none'){
			Dialog.alert('No internet connection.', 'Error');
			return false;
		}

		if (form.$valid == false) {
			Dialog.alert('All fields are required.', 'Error');
			return false;
		}

		var request = {
            email: $scope.data.email_address,
            password: $scope.data.password
        };

    	$ionicLoading.show({
            content: 'Loading',
            showBackdrop: true,
            maxWidth: 50,
            showDelay: 0
        });

        LocalStorage.set('apiurl', $scope.data.url.replace(/\/$/, ''));

        WebApi.sendData(request,'mobilelogin').then( function (result) {
            if(Obj.size(result) > 0){
                var apidata = {
                    api_token: result.api_token,
                    api_key: result.api_key,
                    email: $scope.data.email_address,
                    files_directory: result.files_directory,
                    name: result.name_of_user
                }

                LocalStorage.setObject('apidata', apidata);
                if(result.api_token && result.api_key){
                    console.log(result.api_token +'&&'+ result.api_key);
                    $state.go('search-applicant');
                    return true;
                }
                Dialog.alert('Unable to retrieve data on the server.', 'Error');
                $state.go('login');
            } else {
                $timeout(function () {
                    $ionicLoading.hide();
                }, 1000);
                Dialog.alert('The email and password you entered is incorrect.', 'Error');
            }
        }, function (error){
            $ionicLoading.hide();
            Dialog.alert('Connection error. Please try again.', 'Error');
        })
	}
})
.controller('IntroCtrl', function ($scope, $state, Obj, LocalStorage){
	var result = LocalStorage.getObject('apidata');
    if(Obj.size(result) > 0){
        var url = LocalStorage.get('apiurl') || 'https://www.myra.work';
        LocalStorage.set('apiurl', url);
        $state.go('search-applicant');
    } else {
        $state.go('login');
    }
})
.controller('ApplicantCtrl', function ($scope, $rootScope, $state, $stateParams, $ionicLoading, $ionicHistory, $cordovaCamera, $cordovaFile, $cordovaNetwork, $timeout, Obj, WebApi, Dialog, LocalStorage){
	document.addEventListener("deviceready", function () {
        /* Listener for syncing when device become online */
        var type = $cordovaNetwork.getNetwork();
        $scope.online = type;
        if(type == 'none'){
        	Dialog.alert('No internet connection.', 'Error');
        }

        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            $scope.online = networkState;
        });

        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
          	$scope.online = networkState;
          	$ionicLoading.show({
                content: 'Loading',
                showBackdrop: true,
                maxWidth: 50,
                showDelay: 0
            });
          	Dialog.alert('Connection lost.', 'Error');
          	$timeout(function () {
                $ionicLoading.hide();
            }, 1000);
        });

    }, false);

	var searchtype = LocalStorage.getObject('searchtype');
	var result = LocalStorage.getObject('apidata');
	$scope.data = {};
	$scope.searchtype = 'app_name';
	$scope.placeholder = 'Search Applicant Name';
	$scope.numberOfItemsToDisplay = 5;
	$scope.directory = 'http://myra-development-bucket.s3-us-west-2.amazonaws.com/'+result.files_directory+'/attachments';

	if($ionicHistory.backView() !== null) { 
        if ($ionicHistory.backView().stateName == 'intro' || $ionicHistory.backView().stateName == 'login' ) {
            $ionicHistory.clearHistory();
        }
    }

    $scope.appname = function() {
    	$scope.searchtype = 'app_name';
    	$scope.data.query = '';
    	$scope.placeholder = 'Search Applicant Name';
    }

    $scope.appnumber = function() {
    	$scope.searchtype = 'app_number';
    	$scope.data.query = '';
    	$scope.placeholder = 'Search Applicant Number';
    }

    $scope.searchfield = function(){
    	$ionicLoading.show({
            content: 'Loading',
            showBackdrop: true,
            maxWidth: 50,
            showDelay: 0
        });
        $scope.data.query = '';
        $scope.applicants = [];
        $timeout(function () {
            $ionicLoading.hide();
        }, 1000);
    }

    $scope.search = function() {
    	var request = {
            query: $scope.data.query,
            searchtype: $scope.searchtype,
            token: result.api_token,
            key: result.api_key
        };

        if($scope.data.query != undefined){
            WebApi.sendData(request,'searchapplicant').then( function (result) {
                if(Obj.size(result) > 0){
                    $scope.applicants = result;
                } else {
                    Dialog.alert('No record found.', 'Error');
                }
            }, function(error){
            	Dialog.alert('No internet connection.', 'Error');
            })
        } else {
        	Dialog.alert('All fields are required.', 'Error');
        }
    }

    $scope.addMoreItem = function(done) {
	    if ($scope.applicants.length > $scope.numberOfItemsToDisplay){
	        $scope.numberOfItemsToDisplay += 5; // load number of more items
	        $scope.$broadcast('scroll.infiniteScrollComplete');
	    }
	}
	
	$scope.takePhoto = function (applicant_id) {
        document.addEventListener("deviceready", function () {
	        var options = {
	            quality: 75,
	            destinationType: Camera.DestinationType.DATA_URL,
	            sourceType: Camera.PictureSourceType.CAMERA,
	            allowEdit: true,
	            encodingType: Camera.EncodingType.JPEG,
	            targetWidth: 300,
	            targetHeight: 300,
	            popoverOptions: CameraPopoverOptions,
	            saveToPhotoAlbum: false
	        };

	        $cordovaCamera.getPicture(options).then(function (imageData) {
	            var request = {
                	applicant_id: applicant_id,
                	user_email: result.email,
                	token: result.api_token,
        			key: result.api_key,
                    photo: imageData
                }
                
                WebApi.sendData(request,'photocapture').then( function (result) {
	                if(Obj.size(result) > 0){
	                    $scope.photo = result;
	                    Dialog.alert('2x2 Picture has been uploaded.', 'Success');
	                    if($stateParams.id){
	                    	$scope.profile($stateParams.id);
	                    	return false;
	                    }
	                    $scope.search();
	                } else {
	                    $timeout(function () {
	                        $ionicLoading.hide();
	                    }, 1000);
	                }
	            })
	        });
	    }, false);
    }

    $scope.takeWBPhoto = function (applicant_id) {
        document.addEventListener("deviceready", function () {
	        var options = {
	            quality: 75,
	            destinationType: Camera.DestinationType.DATA_URL,
	            sourceType: Camera.PictureSourceType.CAMERA,
	            allowEdit: false,
	            encodingType: Camera.EncodingType.JPEG,
	            targetWidth: 512,
	            targetHeight: 512,
	            popoverOptions: CameraPopoverOptions,
	            saveToPhotoAlbum: false,
                correctOrientation: true
	        };

	        $cordovaCamera.getPicture(options).then(function (imageData) {
	            var request = {
                	applicant_id: applicant_id,
                	user_email: result.email,
                	token: result.api_token,
        			key: result.api_key,
                    photo: imageData
                }
                
                WebApi.sendData(request,'wbphotocapture').then( function (result) {
	                if(Obj.size(result) > 0){
	                    $scope.photo = result;
	                    Dialog.alert('Whole Body Picture has been uploaded.', 'Success');
	                    if($stateParams.id){
	                    	$scope.profile($stateParams.id);
	                    	return false;
	                    }
	                    $scope.search();
	                } else {
	                    $timeout(function () {
	                        $ionicLoading.hide();
	                    }, 1000);
	                }
	            })
	        });
	    }, false);
    }

    $scope.profile = function(applicant_id){
    	var request = {
        	applicant_id: applicant_id,
        	user_email: result.email,
        	token: result.api_token,
			key: result.api_key
        }
    	WebApi.sendData(request,'getapplicant').then( function (result) {
    		$ionicLoading.show({
                content: 'Loading',
                showBackdrop: true,
                maxWidth: 50,
                showDelay: 0
            });
    		$timeout(function () {
                $ionicLoading.hide();
            	$scope.applicantdata = result;
            }, 1000);
    	});

    }

    $scope.refresher = function(){
    	$scope.profile($stateParams.id);
    }

    if($stateParams.id){
    	$scope.applicant_id = $stateParams.id;
    	$scope.profile($stateParams.id);
    }
})
.controller('UserCtrl', function ($scope, $state, $ionicLoading, $ionicHistory, $timeout, Obj, LocalStorage){
	var result = LocalStorage.getObject('apidata');
	$scope.userdisplay = function(){
		$scope.user = result;
	}

	$scope.logout = function(){

        $scope.$on('$ionicView.enter', function(event, viewData) {
            $ionicHistory.clearCache();
        });

		$ionicLoading.show({
            content: 'Loading',
            showBackdrop: true,
            maxWidth: 50,
            showDelay: 0
        });
		LocalStorage.remove('apidata');
		$timeout(function () {
            $ionicLoading.hide();
            $ionicHistory.clearCache();
          	$ionicHistory.clearHistory();
  /*          ionic.Platform.exitApp(); // stops the app
            window.close();*/
             $state.go('login');
        }, 2000);
	}

	$scope.userdisplay();
});