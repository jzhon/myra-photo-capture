/**
* mwsng Module
*
* Description
*/
angular.module('web-api', ['starter.services'])

.factory('WebApi', function(
    $http, 
    $q,
    $ionicLoading,
    $state,
    $rootScope,
    $ionicActionSheet,
    Network,
    Dialog,
    LocalStorage,
    Toast,
    transformRequestAsFormPost){

   // var rootUrl = $rootScope.url = LocalStorage.get('apiurl') || 'https://www.myra.work/client/connect/';
   // var localUrl = 'http://192.168.2.102:8000/client/connect/'; /* sample for testing  http://192.168.2.107:8000/client/connect/ */
   
   // var url = rootUrl;

    function getData(id){
        var baseurl = LocalStorage.get('apiurl') || 'https://www.myra.work';
        var url = baseurl.replace(/\/$/, '') + '/client/connect/';
        var deferred = $q.defer();
        $ionicLoading.show({showBackdrop: false,});
         $http.get(url + "getapplicant", {params:{id:id}})
        .success(function(data, status, headers, config){
            deferred.resolve(data);
        })
        .error(function(data, status){
            deferred.reject(data);
        })
        .finally(function(){
            $ionicLoading.hide();
        });
        return deferred.promise;
    }


    function sendData(params,what)
    {
        var baseurl = LocalStorage.get('apiurl') || 'https://www.myra.work';
        var url = baseurl.replace(/\/$/, '') + '/client/connect/';
        var deferred = $q.defer();
        $ionicLoading.show({showBackdrop: false,});

        $http({
            url: url+what,
            data:params,
            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
            method:'POST',
            transformRequest: transformRequestAsFormPost
        })

        .success(function(data, status, headers, config){

            var dataResolve,
                details = data.details;
            deferred.resolve(data);
        })

        .error(function(data, status, headers, config){
            // Toast.show('Submission failed: No network connection', 1000, 'bottom');
            deferred.reject(data);
        })

        .finally(function(){
            $ionicLoading.hide();
        });
        return deferred.promise;
    }

    return {
        getData:getData,
        sendData:sendData,
    }

})